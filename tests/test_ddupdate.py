import json
import subprocess
from ipaddress import IPv6Address
from unittest.mock import Mock

import pytest
import requests

import ddupdate


@pytest.fixture
def mock_response():
    response = Mock()
    response.status_code = 200
    response.text = "good"
    return response


def test_get_ip_success(monkeypatch):
    expected_ip = "2001:9e8:69e3:f000:6dd5:6794:5d1b:a06f"
    ip_response = json.dumps(
        [
            {
                "addr_info": [
                    {},
                    {"local": expected_ip, "temporary": True},
                    {"local": expected_ip},
                ]
            }
        ]
    )

    monkeypatch.setattr(
        subprocess,
        "run",
        lambda x, **kwargs: subprocess.CompletedProcess(x, 0, ip_response),
    )
    assert ddupdate.get_ip("eth0") == IPv6Address(expected_ip)


def test_get_ip_failure(monkeypatch):
    monkeypatch.setattr(
        subprocess, "run", lambda x, **kwargs: subprocess.CompletedProcess(x, 1, "", "")
    )
    with pytest.raises(ddupdate.NoIpError):
        ddupdate.get_ip("eth0")


def test_get_ip_no_global_ip(monkeypatch):
    ip_response = json.dumps([{"addr_info": [{"local": "fe80::2e0:4cff:fe8d:c3b6"}]}])
    monkeypatch.setattr(
        subprocess,
        "run",
        lambda x, **kwargs: subprocess.CompletedProcess(x, 0, ip_response),
    )
    with pytest.raises(ddupdate.NoIpError):
        ddupdate.get_ip("eth0")


def test_update_ip_success(monkeypatch, mock_response):
    monkeypatch.setattr(requests, "get", lambda *args, **kwargs: mock_response)
    ddupdate.update_ip(
        IPv6Address("::1"),
        "example.com",
        "user",
        "pass",
        "http://dynupdate.no-ip.com/nic/update",
    )

    mock_response.text = "nochg"
    monkeypatch.setattr(requests, "get", lambda *args, **kwargs: mock_response)
    ddupdate.update_ip(
        IPv6Address("::1"),
        "example.com",
        "user",
        "pass",
        "http://dynupdate.no-ip.com/nic/update",
    )


def test_update_ip_fail(monkeypatch, mock_response):
    mock_response.text = "bad auth"
    monkeypatch.setattr(requests, "get", lambda *args, **kwargs: mock_response)
    with pytest.raises(ddupdate.IpUpdateError) as e:
        ddupdate.update_ip(
            IPv6Address("::1"),
            "example.com",
            "user",
            "pass",
            "http://dynupdate.no-ip.com/nic/update",
        )
    assert str(e.value) == "IP update failed: bad auth"


def test_update_ip_request_exception(monkeypatch):
    def raise_request(*args, **kwargs):
        raise requests.exceptions.RequestException()

    monkeypatch.setattr(requests, "get", raise_request)

    with pytest.raises(ddupdate.IpUpdateError) as e:
        ddupdate.update_ip(
            IPv6Address("::1"),
            "example.com",
            "user",
            "pass",
            "http://dynupdate.no-ip.com/nic/update",
        )
    assert str(e.value) == "IP update failed: "


def test_main(monkeypatch, mock_response):
    ip_response = json.dumps(
        [{"addr_info": [{"local": "2001:9e8:69e3:f000:6dd5:6794:5d1b:a06f"}]}]
    )
    monkeypatch.setattr(
        subprocess,
        "run",
        lambda x, **kwargs: subprocess.CompletedProcess(x, 0, ip_response),
    )
    monkeypatch.setattr(requests, "get", lambda *args, **kwargs: mock_response)
    monkeypatch.setattr(ddupdate, "running", False)

    ddupdate.main(args=[])


def test_main_noiperror(monkeypatch, mock_response):
    monkeypatch.setattr(
        subprocess, "run", lambda x, **kwargs: subprocess.CompletedProcess(x, 1, "", "")
    )
    monkeypatch.setattr(requests, "get", lambda *args, **kwargs: mock_response)
    monkeypatch.setattr(ddupdate, "running", False)

    ddupdate.main(args=[])


def test_main_ipupdateerror(monkeypatch, mock_response):
    ip_response = json.dumps(
        [{"addr_info": [{"local": "2001:9e8:69e3:f000:6dd5:6794:5d1b:a06f"}]}]
    )
    monkeypatch.setattr(
        subprocess,
        "run",
        lambda x, **kwargs: subprocess.CompletedProcess(x, 0, ip_response),
    )
    mock_response.text = "foo"
    monkeypatch.setattr(requests, "get", lambda *args, **kwargs: mock_response)
    monkeypatch.setattr(ddupdate, "running", False)

    ddupdate.main(args=[])
